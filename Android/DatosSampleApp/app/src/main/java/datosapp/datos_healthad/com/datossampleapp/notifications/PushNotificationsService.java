package datosapp.datos_healthad.com.datossampleapp.notifications;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import datos_health.com.sdk.app.services.DatosHealthPushMessagesDelegate;
import datosapp.datos_healthad.com.datossampleapp.MainActivity;

public class PushNotificationsService extends FirebaseMessagingService {

        private DatosHealthPushMessagesDelegate datosHealthPushMessagesDelegate;

        @Override
        public void onCreate() {
            super.onCreate();
            datosHealthPushMessagesDelegate = new DatosHealthPushMessagesDelegate(this, MainActivity.class, null);
            datosHealthPushMessagesDelegate.onCreate();
        }

        @Override
        public void onNewToken(@NonNull String s) {
            super.onNewToken(s);
            datosHealthPushMessagesDelegate.onNewToken(s);
        }

        @Override
        public void onMessageReceived(RemoteMessage remoteMessage) {
            datosHealthPushMessagesDelegate.onMessageReceived(remoteMessage);
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            datosHealthPushMessagesDelegate.onDestroy();
        }
}
