package datosapp.datos_healthad.com.datossampleapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.android.volley.VolleyError;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.view.View;

import datos_health.com.sdk.DatosHealthSDK;
import datos_health.com.sdk.OnFinishDatosHealthListener;
import datos_health.com.sdk.app.api.DatosApiResponseListener;
import datos_health.com.sdk.app.api.DatosBaseApi;
import datos_health.com.sdk.app.common.Constants;
import datos_health.com.sdk.app.data.app_models.LoginResponse;
import datos_health.com.sdk.app.data.server_models.LoginResponseSM;
import datos_health.com.sdk.app.data.server_models.ResponseErrorSM;
import datos_health.com.sdk.app.utils.DatosSDKLogUtils;
import datos_health.com.sdk.app.utils.KeyboardUtils;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private String authToken;
    private String refreshToken;
    private ProgressBar progressBar;
    private boolean sdkInitiated;
    private boolean canShowSDKUI;
    private final BroadcastReceiver refreshTokenErrorReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showError("Refresh of Authentication Token failed, please login again");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Button login = findViewById(R.id.loginButton);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });

        Button populateUserData = findViewById(R.id.populateUserData);
        populateUserData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateUserData();
            }
        });

        Button registerToFirebase = findViewById(R.id.registerToFirebase);
        registerToFirebase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registerToFirebase();
            }
        });

        Button showSdkActivity = findViewById(R.id.showSdkActivity);
        showSdkActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSdkActivity();
            }
        });

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        LocalBroadcastManager.getInstance(this)
                .registerReceiver(refreshTokenErrorReceiver, new IntentFilter(Constants.REFRESH_TOKEN_ERROR));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(refreshTokenErrorReceiver);
        super.onDestroy();
    }

    private void login() {
         EditText usernameET = findViewById(R.id.usernameET);
         EditText passwordET = findViewById(R.id.passwrodET);
         if (usernameET.getText() == null || usernameET.getText().length() == 0 || passwordET.getText() == null || passwordET.getText().length() == 0) {
             showError("Need username and password for login into the server");
             return;
         }
         String username = String.valueOf(usernameET.getText());
         String password = String.valueOf(passwordET.getText());
         String serverUrl = "https://lab.datosconnectedhealth.com";

         progressBar.setVisibility(View.VISIBLE);
         if (!sdkInitiated) {
             DatosHealthSDK.initialize(this, serverUrl, null, "en", false, false, false, getString(R.string.app_name));

             TextView versionTv = findViewById(R.id.versionTextView);
             versionTv.setText("Version " + DatosHealthSDK.getVersionNumber());
             versionTv.setVisibility(View.VISIBLE);
             sdkInitiated = true;
             DatosSDKLogUtils.initialize(null, true);
         }

         if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
             showError("No username or password to login to server");
             progressBar.setVisibility(View.GONE);
             KeyboardUtils.hideKeyboard(this);
             return;
         }
         KeyboardUtils.hideKeyboard(this);
        new DatosBaseApi(this, null).sendLoginRequest(username, password, new DatosApiResponseListener() {
            @Override
            public void onServerError(ResponseErrorSM error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Login to Datos server failed", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "Login to Datos server failed", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onResponse(Object response) {
                progressBar.setVisibility(View.GONE);
                LoginResponse loginResponse = new LoginResponse((LoginResponseSM) response);
                if (loginResponse.isSuccess()) {
                    authToken = loginResponse.getToken();
                    refreshToken = loginResponse.getRefreshToken();
                    Toast.makeText(getApplicationContext(), "Logged into Datos server successfully", Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(), "Login to Datos server failed", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void populateUserData() {
        if (authToken == null || authToken.isEmpty()) {
            showError("No tokens to communicate with Datos server");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        DatosHealthSDK.populateUserDataAndConfiguration(authToken, refreshToken, new OnFinishDatosHealthListener() {
            @Override
            public void onSucceess() {
                progressBar.setVisibility(View.GONE);
                canShowSDKUI = true;
                Toast.makeText(getApplicationContext(), "User data populated successfully", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(DatosHealthSDK.ErrorCodes s) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "User data failed to populate "+s, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void registerToFirebase() {
        if (authToken == null || authToken.isEmpty()) {
            showError("No tokens to communicate with Datos server");
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(new OnCompleteListener<String>() {
                    @Override
                    public void onComplete(@NonNull Task<String> task) {
                        if (!task.isSuccessful()) {
                            Toast.makeText(MainActivity.this, "Fetching FCM registration token failed " + task.getException(), Toast.LENGTH_SHORT).show();
                            return;
                        }

                        // Get new FCM registration token
                        String token = task.getResult();
                        DatosHealthSDK.registerForNotifications(token, new OnFinishDatosHealthListener() {
                            @Override
                            public void onSucceess() {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(MainActivity.this, "FCM Token sent to server successfully", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onFailure(DatosHealthSDK.ErrorCodes s) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(MainActivity.this, "Failed to send FCM Token to server", Toast.LENGTH_SHORT).show();
                            }
                        });
                        // Log and toast
                        Toast.makeText(MainActivity.this, "FCM Token sent to server", Toast.LENGTH_SHORT).show();
                    }
                });
    }

     private void showSdkActivity() {
         if (!canShowSDKUI) {
             showError("User data wasn't populated so UI cannot be shown");
             return;
         }
         progressBar.setVisibility(View.VISIBLE);
         DatosHealthSDK.show(new OnFinishDatosHealthListener() {
            @Override
            public void onSucceess() {
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(DatosHealthSDK.ErrorCodes s) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getApplicationContext(), "SDK failed to show "+s, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showError(String message) {
        AlertDialog.Builder dialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        dialog.create().show();
    }
 }