### Quick summary ###
This sample app runs basic functionality of the Datos SDK. Currently only Android folder has content with the Datos Android SDK sample app.

Version 1.0

### How do I get set up? ###

### For Android ###

- Copy the datos-sdh.aar file from its shared location into an app/libs folder.

- Inside the sample app's MainActivity class there is a function called "login". In that function you need to define the server URL you are working with - replace the current value in the serverUrl parameter.

Now you can just run the app.

### Who do I talk to? ###

* idit.barash@datos-health.com
